use std::fmt;
use std::fmt::Debug;
use std::fs::File;
use std::io::Write;
use std::io::{BufRead, BufReader};

//Contient les orientations possibles du robot
enum Orientation {
    N,
    S,
    E,
    O,
}
//Permet l'affichage de l'orientation
impl std::fmt::Display for Orientation {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let i = match self {
            Orientation::N => "N",
            Orientation::S => "S",
            Orientation::E => "E",
            Orientation::O => "O",
        };
        write!(f, "{}", i)
    }
}
//Contient les instructions possibles du robot
enum Instruction {
    L,
    R,
    F,
}
//Permet l'affichage des instructions
impl std::fmt::Display for Instruction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let i = match self {
            Instruction::L => "L",
            Instruction::R => "R",
            Instruction::F => "F",
        };
        write!(f, "{}", i)
    }
}
//Representation du robot
struct Robot {
    /// x : represente sa position en abscisse
    /// y : represente sa position en ordonnées
    /// orient: represente l'orientation
    /// deplacement : represente les mouvements a effectué
    /// id : identifiant du robot
    x: i32,
    y: i32,
    orient: Orientation,
    deplacement: Vec<Instruction>,
    id: i16,
}
impl Robot {
    // Création d'un robot avec les valeurs d'initialisation
    fn new(x: i32, y: i32, orient: Orientation, deplacement: Vec<Instruction>, id: i16) -> Robot {
        Robot {
            x,
            y,
            orient,
            deplacement,
            id,
        }
    }

    //Affecte le champs de déplacement du robot
    fn affecte_deplacement(&mut self, depl: Vec<Instruction>) {
        self.deplacement = depl;
    }

    //Affiche les informations du robot
    fn affiche(&self) {
        println!("ROBOT {}: ", self.id);
        println!("\tx: {}", self.x);
        println!("\ty: {}", self.y);
        println!("\torientation: {}", self.orient);

        if self.deplacement.len() != 0 {
            let _elt: Instruction;
            print!("\tinstruction: ");

            for elt in &self.deplacement {
                print!("{}", elt);
            }

            println!("");
        }
    }

    //Indique si le robot a encore un déplacement a faire
    fn existe_encore_deplacement(&self) -> bool {
        if self.deplacement.len() == 0 {
            return false;
        } else {
            return true;
        }
    }

    //Renvoie le déplacement a traiter
    fn lire_deplacement_courant(&mut self) -> Instruction {
        self.deplacement.remove(0)
    }

    //Indique si un robot est au meme emplacement qu'un autre
    fn collision(&self, robot2: &Robot) -> bool {
        self.x == robot2.x && self.y == robot2.y
    }

    //Change l'orientation et les coordonnées du robot selon son instruction courante
    fn bouge(&mut self) {
        let instruction_courante: Instruction;
        instruction_courante = self.lire_deplacement_courant();

        match instruction_courante {
            Instruction::R => match self.orient {
                Orientation::N => self.orient = Orientation::E,
                Orientation::S => self.orient = Orientation::O,
                Orientation::O => self.orient = Orientation::N,
                Orientation::E => self.orient = Orientation::S,
            },
            Instruction::L => match self.orient {
                Orientation::O => self.orient = Orientation::S,
                Orientation::E => self.orient = Orientation::N,
                Orientation::N => self.orient = Orientation::O,
                Orientation::S => self.orient = Orientation::E,
            },
            Instruction::F => match self.orient {
                Orientation::O => self.x = self.x - 1,
                Orientation::E => self.x = self.x + 1,
                Orientation::N => self.y = self.y + 1,
                Orientation::S => self.y = self.y - 1,
            },
        }
    }
}
//Representation de la grille
struct Grille {
    xmax: i32,
    ymax: i32,
}
impl Grille {
    /// Initialisation la taille de la grille
    fn new(xmax: i32, ymax: i32) -> Grille {
        Grille { xmax, ymax }
    }
    // Affiche les information de la grille
    fn affiche(&self) {
        println!("LA GRILLE:");
        println!("\tnombre de lignes: {}", self.xmax);
        println!("\tnombre de colonnes: {}", self.ymax);
    }

    // Indique si le x et y sont tjrs dans la grille
    fn toujours_dans_grille(&self, robot: &Robot) -> bool {
        self.xmax >= robot.x && self.ymax >= robot.y
    }
}
//Fonction qui permet de transformer un caractere en une instruction
#[derive(Debug)]
struct ErrorInvalidInstruction;
fn parse_instruction(instr: char) -> Result<Instruction, ErrorInvalidInstruction> {
    match instr {
        'F' => Ok(Instruction::F),
        'L' => Ok(Instruction::L),
        'R' => Ok(Instruction::R),
        _ => Err(ErrorInvalidInstruction),
    }
}
//Fonction qui permet de transformer un caractere en une orientation
#[derive(Debug)]
struct ErrorInvalidOrientation;
fn parse_orientation(instr: &str) -> Result<Orientation, ErrorInvalidOrientation> {
    match instr {
        "N" => Ok(Orientation::N),
        "S" => Ok(Orientation::S),
        "O" => Ok(Orientation::O),
        "E" => Ok(Orientation::E),
        _ => Err(ErrorInvalidOrientation),
    }
}

//FCT lire_monde lis le fichier two_robots.txt avec les paramètre d'initialisation du monde
// retourne un tupple avec la grille et robot 1 et 2 initialisés
fn lire_monde() -> (Grille, Robot, Robot) {
    let mut robot1: Robot;
    let mut robot2: Robot;
    let mut ma_grille: Grille;

    ma_grille = Grille::new(0, 0);
    robot1 = Robot::new(0, 0, Orientation::N, vec![], 1);
    robot2 = Robot::new(0, 0, Orientation::N, vec![], 2);

    let nom_fichier = "two_robots.txt";
    // Ouvrir le fichier et lire dedans
    let file = File::open(nom_fichier).unwrap();
    let reader = BufReader::new(file);
    let mut index = 0;

    for line in reader.lines() {
        let line = line.unwrap();
        if line.len() == 0 {
            continue;
        }
        match index {
            // Extraire les mots d'une ligne pour initialiser la grille le robot 1 et robot2
            0 => {
                // chaine est une variable qui retourne un vecteur de string
                let chaine: Vec<&str> = line.split(" ").collect();

                let xmax: i32 = chaine[0].parse().unwrap_or(0);
                let ymax: i32 = chaine[1].parse().unwrap_or(0);

                // initialisation de la structure grille
                ma_grille = Grille::new(xmax, ymax);

                index += 1;
            }
            1 => {
                // chaine est une variable qui retourne un vecteur de string
                let chaine: Vec<&str> = line.split(" ").collect();

                let rob1_x: i32 = chaine[0].parse().unwrap_or(0);
                let rob1_y: i32 = chaine[1].parse().unwrap_or(0);

                let rob1_orientation = chaine[2];
                let rob1_orient = match parse_orientation(rob1_orientation) {
                    Ok(val) => val,
                    Err(error) => panic!("Probleme d'orientation : {:?}", error),
                };
                robot1 = Robot::new(rob1_x, rob1_y, rob1_orient, vec![], 1);
                index += 1;
            }

            2 => {
                let mut rob1_depl = Vec::new();

                for element in line.chars() {
                    let inst_elt = match parse_instruction(element) {
                        Ok(val) => val,
                        Err(error) => panic!("Probleme d'instruction : {:?}", error),
                    };

                    rob1_depl.push(inst_elt);
                }
                robot1.affecte_deplacement(rob1_depl);
                index += 1;
            }

            3 => {
                // chaine est une variable qui retourne un vecteur de string
                let chaine: Vec<&str> = line.split(" ").collect();

                let rob2_x: i32 = chaine[0].parse().unwrap_or(0);
                let rob2_y: i32 = chaine[1].parse().unwrap_or(0);
                let rob2_orientation = chaine[2];
                let rob2_orient = match parse_orientation(rob2_orientation) {
                    Ok(val) => val,
                    Err(error) => panic!("Probleme d'orientation : {:?}", error),
                };

                robot2 = Robot::new(rob2_x, rob2_y, rob2_orient, vec![], 2);
                index += 1;
            }
            4 => {
                let mut rob2_depl = Vec::new();
                for element in line.chars() {
                    let inst_elt = match parse_instruction(element) {
                        Ok(val) => val,
                        Err(error) => panic!("Probleme d'instruction : {:?}", error),
                    };

                    rob2_depl.push(inst_elt);
                }
                robot2.affecte_deplacement(rob2_depl);
            }
            _ => {
                writeln!(std::io::stderr(), "CAS INCONNU NON TRAITER {}", line).unwrap();
                std::process::exit(1);
            }
        }
    }
    (ma_grille, robot1, robot2)
}
//Fonction qui permet d'animer les robots en fonction de leurs instructions et de la grille
fn traiter_monde(ma_grille: &Grille, robot1: &mut Robot, robot2: &mut Robot) {
    let mut res1: bool;
    let mut res2: bool;

    res1 = robot1.existe_encore_deplacement();
    res2 = robot2.existe_encore_deplacement();

    while res1 || res2 {
        if res1 != false {
            robot1.bouge();

            if ma_grille.toujours_dans_grille(robot1) {
                if robot1.collision(robot2) {
                    writeln!(std::io::stderr(), "COLLISON ENTRE LES DEUX ROBOT").unwrap();
                    std::process::exit(1);
                }
            } else {
                writeln!(std::io::stderr(), "ROBOT 1 SORTIE DE LA GRILLE").unwrap();
                std::process::exit(1);
            }
        }
        if res2 != false {
            robot2.bouge();

            if ma_grille.toujours_dans_grille(robot2) {
                if robot2.collision(robot1) {
                    writeln!(std::io::stderr(), "COLLISON ENTRE LES DEUX ROBOT").unwrap();
                    std::process::exit(1);
                }
            } else {
                writeln!(std::io::stderr(), "ROBOT 2 SORTIE DE LA GRILLE").unwrap();
                std::process::exit(1);
            }
        }

        res1 = robot1.existe_encore_deplacement();
        res2 = robot2.existe_encore_deplacement();
    }
}
//Fonction principal pour lancer le traitement du jeu
fn main() {
    println!(" ====================================");
    println!("\t<< Lisons le monde  >>");
    println!(" ====================================");
    let (ma_grille, mut robot1, mut robot2) = lire_monde();
    if ma_grille.toujours_dans_grille(&robot1) {
        if robot1.collision(&robot2) {
            writeln!(std::io::stderr(), "COLLISON ENTRE LES DEUX ROBOT").unwrap();
            std::process::exit(1);
        }
    }

    println!("\t\t\tETAT INITIAL DU MONDE ");
    ma_grille.affiche();
    robot1.affiche();
    robot2.affiche();

    println!(" ====================================");
    println!("\t<< Traitons le monde >>");
    println!(" ====================================");

    traiter_monde(&ma_grille, &mut robot1, &mut robot2);

    println!("\t\t\tETAT FINAL DU MONDE ");
    ma_grille.affiche();
    robot1.affiche();
    robot2.affiche();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_grille() {
        let grille: Grille = Grille::new(10, 10);
        assert_eq!(grille.xmax, 10);
        assert_eq!(grille.ymax, 10);
    }
    #[test]
    fn test_Robot() {
        let robot: Robot = Robot::new(1, 1, Orientation::N, vec![], 2);
        assert!(matches!(robot.x, 1));
        assert!(matches!(robot.y, 1));
        assert!(matches!(robot.orient, Orientation::N));
        assert!(matches!(robot.id, 2));
    }

    #[test]
    fn test_affecte_deplacement() {
        let mut robot = Robot::new(
            1,
            1,
            Orientation::N,
            vec![
                Instruction::F,
                Instruction::L,
                Instruction::R,
                Instruction::F,
            ],
            2,
        );
        robot.affecte_deplacement(vec![
            Instruction::R,
            Instruction::F,
            Instruction::L,
            Instruction::F,
        ]);
        assert_eq!(
            robot.affecte_deplacement(vec![
                Instruction::R,
                Instruction::F,
                Instruction::L,
                Instruction::F
            ]),
            ()
        );
    }
    #[test]
    #[should_panic]
    fn test_collision() {
        let mut robot1 = Robot::new(
            0,
            1,
            Orientation::O,
            vec![
                Instruction::F,
                Instruction::L,
                Instruction::L,
                Instruction::F,
                Instruction::R,
                Instruction::F,
            ],
            2,
        );
        let mut robot2 = Robot::new(
            3,
            0,
            Orientation::O,
            vec![
                Instruction::F,
                Instruction::F,
                Instruction::L,
                Instruction::F,
                Instruction::R,
                Instruction::R,
                Instruction::F,
            ],
            3,
        );
        assert!(&robot1.collision(&robot2));
    }
    #[test]
    fn test_traiter_monde() {
        let mut robot1 = Robot::new(
            0,
            1,
            Orientation::O,
            vec![
                Instruction::F,
                Instruction::L,
                Instruction::L,
                Instruction::F,
                Instruction::R,
                Instruction::F,
            ],
            2,
        );
        let mut robot2 = Robot::new(
            3,
            2,
            Orientation::S,
            vec![
                Instruction::F,
                Instruction::F,
                Instruction::L,
                Instruction::F,
                Instruction::R,
                Instruction::R,
                Instruction::F,
            ],
            3,
        );
        let grille: Grille = Grille::new(5, 5);
        assert_eq!(traiter_monde(&grille, &mut robot1, &mut robot2), ());
    }
}
